package com.hackathon.bbvahotdesk.servicio.impl;



import com.hackathon.bbvahotdesk.modelo.ModeloEscritorio;
import com.hackathon.bbvahotdesk.servicio.RepositorioEscritorio;
import com.hackathon.bbvahotdesk.servicio.ServicioEscritorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioEscritorioImpl implements ServicioEscritorio {

   @Autowired
    RepositorioEscritorio  repositorioEscritorio;

   @Override
   public void apartarEscritorio(ModeloEscritorio m){
       m.setId(null);
       this.repositorioEscritorio.insert(m);
   }


   @Override
   public ModeloEscritorio buscarPorId(String id){
       Optional<ModeloEscritorio> p = this.repositorioEscritorio.findById(id);
       return p.isPresent() ? p.get() : null;

   }

    @Override
    public void guardarEstadoPorId(String id, ModeloEscritorio n) {
        n.setId(id);
        this.repositorioEscritorio.save(n);
    }

    @Override
   public List<ModeloEscritorio> obtenerTodos(){

       return this.repositorioEscritorio.findAll();
    }




}
