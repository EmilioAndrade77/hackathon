package com.hackathon.bbvahotdesk.servicio.impl;

import com.hackathon.bbvahotdesk.modelo.ModeloPiso;
import com.hackathon.bbvahotdesk.servicio.RepositorioPiso;
import com.hackathon.bbvahotdesk.servicio.ServicioPiso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;

@Service
public class ServicioPisoImpl implements ServicioPiso {
 @Autowired
 RepositorioPiso repositorioPiso;



 @Override
 public List<ModeloPiso> obtenerTodosLosPisos() {
  return this.repositorioPiso.findAll();
 }




}
