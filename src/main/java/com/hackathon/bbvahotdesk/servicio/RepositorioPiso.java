package com.hackathon.bbvahotdesk.servicio;
import com.hackathon.bbvahotdesk.modelo.ModeloPiso;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioPiso extends MongoRepository<ModeloPiso, String> {

}