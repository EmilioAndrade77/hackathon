package com.hackathon.bbvahotdesk.servicio;

import com.hackathon.bbvahotdesk.modelo.ModeloEdificio;

import java.util.List;

public interface ServicioEdificio {

    public List<ModeloEdificio> obtenerTodos(String field);

}
