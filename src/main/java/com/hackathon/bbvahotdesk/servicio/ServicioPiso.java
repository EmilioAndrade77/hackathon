package com.hackathon.bbvahotdesk.servicio;

import com.hackathon.bbvahotdesk.modelo.ModeloPiso;

import java.util.List;

public interface ServicioPiso {
    public List<ModeloPiso> obtenerTodosLosPisos();

}
