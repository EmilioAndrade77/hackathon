package com.hackathon.bbvahotdesk.servicio;

import com.hackathon.bbvahotdesk.modelo.ModeloEscritorio;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioEscritorio extends MongoRepository<ModeloEscritorio, String> {

}
