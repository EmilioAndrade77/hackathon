package com.hackathon.bbvahotdesk.servicio;

import com.hackathon.bbvahotdesk.modelo.ModeloEdificio;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface RepositorioEdificio extends MongoRepository<ModeloEdificio, String> {

    public List<ModeloEdificio> findDistinctByEdificio(String edificio);
}
