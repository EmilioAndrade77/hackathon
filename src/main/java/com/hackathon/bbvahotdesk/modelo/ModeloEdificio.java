package com.hackathon.bbvahotdesk.modelo;

import java.util.ArrayList;
import java.util.List;

public class ModeloEdificio {
    String id;
    String edificio;
    List<ModeloPiso> pisos = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return edificio;
    }

    public void setNombre(String nombre) {
        this.edificio = nombre;
    }

    public List<ModeloPiso> getPisos() {
        return pisos;
    }

    public void setPisos(List<ModeloPiso> pisos) {
        this.pisos = pisos;
    }
}
