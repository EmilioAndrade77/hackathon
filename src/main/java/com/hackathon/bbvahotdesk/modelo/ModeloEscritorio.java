package com.hackathon.bbvahotdesk.modelo;

import org.springframework.data.annotation.Id;

import java.util.List;

public class ModeloEscritorio {

    @Id String id;
    String nombre;
    Boolean interno;
    //List<ModeloPiso>  piso;
    //List<ModeloEdificio> edificios;
    String statusLugar;



    public String getStatusLugar() {
        return statusLugar;
    }

    public void setStatusLugar(String statusLugar) {
        this.statusLugar = statusLugar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getInterno() {
        return interno;
    }

    public void setInterno(Boolean interno) {
        this.interno = interno;
    }




}
