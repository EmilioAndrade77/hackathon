package com.hackathon.bbvahotdesk.modelo;

import java.util.List;

public class ModeloPiso {
    List<ModeloEdificio> edificio;
    int piso;
    int totalLugares;

    public List<ModeloEdificio> getEdificio() {
        return edificio;
    }

    public void setEdificio(List<ModeloEdificio> edificio) {
        this.edificio = edificio;
    }

    public int getPiso() {
        return piso;
    }

    public void setPiso(int piso) {
        this.piso = piso;
    }

    public int getTotalLugares() {
        return totalLugares;
    }

    public void setTotalLugares(int totalLugares) {
        this.totalLugares = totalLugares;
    }
}
