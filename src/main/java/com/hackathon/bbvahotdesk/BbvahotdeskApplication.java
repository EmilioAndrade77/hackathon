package com.hackathon.bbvahotdesk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BbvahotdeskApplication {

	public static void main(String[] args) {
		SpringApplication.run(BbvahotdeskApplication.class, args);
	}

}
