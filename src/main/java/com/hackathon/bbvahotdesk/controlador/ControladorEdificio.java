package com.hackathon.bbvahotdesk.controlador;

import com.hackathon.bbvahotdesk.modelo.ModeloEdificio;
import com.hackathon.bbvahotdesk.modelo.ModeloPiso;
import com.hackathon.bbvahotdesk.servicio.ServicioEdificio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/edificios")
public class ControladorEdificio {

    @Autowired
    ServicioEdificio servicioEdificio;

    @GetMapping
    public List<ModeloEdificio> obtenerEdificios(){
        String field = "Edificios";
        return this.servicioEdificio.obtenerTodos(field);
    }

}
