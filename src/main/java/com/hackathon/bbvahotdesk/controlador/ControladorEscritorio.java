package com.hackathon.bbvahotdesk.controlador;


import com.hackathon.bbvahotdesk.modelo.ModeloEdificio;
import com.hackathon.bbvahotdesk.modelo.ModeloEscritorio;
import com.hackathon.bbvahotdesk.modelo.ModeloPiso;
import com.hackathon.bbvahotdesk.servicio.RepositorioEscritorio;
import com.hackathon.bbvahotdesk.servicio.ServicioEscritorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.ArithmeticOperators;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;



@RestController
@RequestMapping("escritorios/apartar")
public class ControladorEscritorio {

    @Autowired
    ServicioEscritorio servicioEscritorio;

    @Autowired
    RepositorioEscritorio repositorioEscritorio;


    @GetMapping
    public List<ModeloEscritorio> obtenerCitas(){
        return this.servicioEscritorio.obtenerTodos();
    }


    @PostMapping
    public void apartarEscritorio(@RequestBody ModeloEscritorio m){
        m.setId(null);
        if(m.getStatusLugar().isEmpty()){
            m.setStatusLugar("Ocupado");
            this.servicioEscritorio.apartarEscritorio(m);
        }

    }

    @PutMapping("/{id}")
    public void cambiarEstado(@PathVariable String id,
                              @RequestBody ModeloEscritorio m){
        final ModeloEscritorio n = this.servicioEscritorio.buscarPorId(id);
        if(n == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        else if(n.getStatusLugar().equals("Ocupado")) {
            this.repositorioEscritorio.delete(n);
        }
        else{
            n.setStatusLugar("Ocupado");
            this.servicioEscritorio.guardarEstadoPorId(id,n);
        }



    }

    @GetMapping("/{id}")
    public ModeloEscritorio obtenerEscritorioPorId(@PathVariable String id) {
        return this.servicioEscritorio.buscarPorId(id);
    }

}
