package com.hackathon.bbvahotdesk.controlador;
import com.hackathon.bbvahotdesk.modelo.ModeloPiso;
import com.hackathon.bbvahotdesk.servicio.RepositorioPiso;
import com.hackathon.bbvahotdesk.servicio.ServicioPiso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Pisos")
public class ControladorPiso {
    @Autowired
    ServicioPiso servicioPiso;

    @Autowired
    RepositorioPiso repositorioPiso;



    @GetMapping
    public List<ModeloPiso> obtenerPiso() {

        return this.servicioPiso.obtenerTodosLosPisos();
    }


    @PostMapping
    public void insertarPisos(){

    }
}